var Booking = require("../models/booking.model");
const moment = require('moment');
const nodemailer = require('nodemailer');
const CronJob = require('cron').CronJob;
const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'coder.undefined@gmail.com',
        pass: '@abc1234'
    }
});

var currentTimeP = moment().valueOf() + (15 * 60 * 1000);// giửi trước 15 mi nút!!!
var currentTime = moment().valueOf();

module.exports = {
    cronSenMail: function () {
        new CronJob('0 */5 * * * *', function () {
            Booking.find({$and: [{time: {$gte: currentTime}}, {time: {$lte: currentTimeP}}]})
                .populate('doctor')
                .populate('user')
                .exec(function (err, list) {
                    if (list.length > 0) {
                        var setMailDoctor = new Set(list.map(a => [a.doctor.local.email, a.user.local.email]));
                        var listMailDoctor = [...setMailDoctor].push();
                        sendToListMail(listMailDoctor[0].time);
                    }
                });
        }).start();
    },
    sendMailHasBooking: function (mailDoctor, time) {
        var timeBook = moment(time).add(7,'h').format('YYYY-MM-DD hh:mm');
        var mailOptions = {
            from: 'Doctor Who',
            to: mailDoctor,
            subject: `You have 1 calendar set at ${timeBook}`,
            text: `You have 1 calendar set at ${timeBook}. Check your work list https://dev20-doctor.herokuapp.com/doctor/work_list`
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + mailDoctor);
            }
        });
    },
    sendMailBookingDone: function (mailUser, time) {
        var timeBook = moment(time).add(7,'h').format('YYYY-MM-DD hh:mm');
        var mailOptions = {
            from: 'Doctor Who',
            to: mailUser,
            subject: `Schedule successful!`,
                text: `You have successfully scheduled your visit at ${timeBook}!!`
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + mailDoctor);
            }
        });
    }


};

function sendToListMail(listMail, timeDoctor) {

    var time = timeDoctor - currentTime;
    var mailOptions = {
        from: 'Doctor Who',
        to: listMail,
        subject: 'You have an appointment after ' + moment(time).format('hh:mm') + ' minutes.',
        text: 'You have an appointment at ' + moment(timeDoctor).format('LLLL') +
            '\n. Go to work https://dev20-doctor.herokuapp.com/work_list'
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + listMail);
        }
    });
}


