var Record = require("../models/record.model");
var mongoose = require('mongoose');
var myid = mongoose.Types.ObjectId;

exports.createRecord = function (req, res) {
    var record = new Record({
        symptom: (req.body.symptom).toString(),
        diagnosis: (req.body.diagnosis).toString(),
        medicine: (req.body.medicine).toString(),
        amount: (req.body.amount).toString(),
        using: (req.body.using).toString(),
        createAt: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
        updateAt: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
        deleteAt: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
        status: '1',
    });
    record.user = new mongoose.mongo.ObjectId(req.body.userId);
    record.doctor = new mongoose.mongo.ObjectId(req.body.doctorId);
    record.save();
    res.json(req.body);
};

exports.listRecordAdmin = function (req, res) {
    Record.find({}, function (err, list) {
        res.render("admin/record.ejs", {
            "listRecord": list
        });
    });
};

exports.listRecord = function (req, res) {
    Record.find({})
        .populate('user')
        .populate('doctor')
        .where('user').eq(new mongoose.mongo.ObjectId(req.user.id))
        .exec(function (err, list) {
            console.log(list);
            res.render('client/record.ejs', {
                user: req.user,
                'listRecord': list
            });
        });
};

exports.getRecord = function (req, res) {
    Record.find({userId: req.params.id}, function (err, list) {
        res.render("client/record.ejs", {
            user: req.user,
            "listRecord": list
        });
    });
};


exports.deleteRegister = function (req, res) {
    Booking.findByIdAndRemove(myid(req.params.id), function (err) {
        if (err)
            res.send(err);
        else
            res.redirect(req.get('referer'));
    });
};

exports.updateRegister = function (req, res) {
    Booking.findByIdAndUpdate(req.params.id, req.body, function (err) {
        if (err) {
            res.send(err);
        }
        else {
            res.redirect(req.get('referer'));
        }
    });
};

