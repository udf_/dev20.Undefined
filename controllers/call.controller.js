const User = require('../models/user.model');
const Money = require('../models/money.model');
var mongoose = require('mongoose');

function getDoctorById(id) {
    return User.findOne({_id: id}).exec();
}

module.exports = function (io) {
    var doctorOnlines = {
        list: new Map(),
        count: 0,
        hasOnline() {
            return this.list.size > 0;
        },
        readList() {
            this.list.forEach(function (value, key, map) {
                console.log(`m[${key}] = ${value}`);
            })
        },
        addDoctorReady(id, socketId) {
            var that = this.list;
            getDoctorById(id).then(function (data) {
                if (!that.has(id)) {
                    that.set(id, {
                        id: data._id,
                        specialId: data.doctor.specialtiesId,
                        socketId: socketId
                    });
                }
            })
        },
        removeDoctorReady(id) {
            if (this.list.has(id)) {
                this.list.delete(id);
            }
        },
        getOne(listSpecial) {
            var list = this.list;
            for (let i = 0; i < listSpecial.length; i++) {
                for (const [key, value] of list) {
                    if (value.specialId === listSpecial[i]) {
                        list.delete(key);
                        return value;
                    }
                }
            }
        },
        getFisrt() {
            var key = this.list.keys().next().value;
            var value = this.list.get(key);
            this.list.delete(key);
            return value;
        }, getById(id) {
            return this.list.get(id);
        }
    };

    var soss = [];
    var myQueue = [];

    io.on('connection', function (socket) {
        // listSocket[socket.handshake.query.userId] = socket.id;
        // console.log("loggeduser => " +  socket.handshake.query.userId);

        socket.userId = socket.handshake.query.userId;
        var listSockets = io.sockets.sockets;
        socket.on('CALL', (data) => {
            callToUser(data.friendId, data.myId, 1);
        });

        socket.on('CALL-ISBOOKING', (data) => {
            callToUser(data.friendId, data.myId, 3);
        });

        socket.on('ON_CALL', (data) => {
            io.sockets.emit(data.myId + 'oncall', data);
        });

        socket.on('DOCTOR_DISCONNECT', function (doctorId) {
            doctorOnlines.removeDoctorReady(doctorId);
            io.to(socket.id).emit('DOCTOR_NOT_READY', {isDisconnect: true});
        });

        socket.on('CHAT', (data) => {
            io.sockets.emit(data.friendId + '1', data);
        });

        socket.on('DOCTOR_ONLINE', doctorId => {
            if (doctorOnlines.list.has(doctorId)) {
                io.to(socket.id).emit('DOCTOR_READY');
            }
        });

        socket.on('DOCTOR_CONNECT', (doctorId) => {
            var socketIdOfDoctor = getSocketIdByUserId(doctorId, listSockets);
            if (soss.length > 0) {

                io.to(socketIdOfDoctor).emit('DOCTOR_NOT_READY');
                var idS = soss.shift();
                callToUser(idS, doctorId, 2, true);
            } else if (myQueue.length > 0) {
                io.to(socketIdOfDoctor).emit('DOCTOR_NOT_READY');
                var id = myQueue.shift();
                io.sockets.in('wait').emit('UPDATE_QUEUE', myQueue);
                callToUser(id, doctorId, 1);
            } else {
                io.to(socketIdOfDoctor).emit('DOCTOR_READY');
                socket.join('doctor');
                doctorOnlines.addDoctorReady(doctorId, socketIdOfDoctor);
            }
        });

        socket.on('DOCTOR_REFUSE', (data) => {
            myQueue.splice(2, 0, data.userId);
            io.sockets.emit(data.userId + '2', {
                message: "bác sĩ éo thích m.Vui lòng đợi xíu !"
            });
        });

        socket.on('USER_REFUSE', (data) => {
            var id = myQueue.shift();
            io.sockets.in('wait').emit('UPDATE_QUEUE', myQueue);
        });

        socket.on('USER_OUT_QUEUE', (userId) => {
            myQueue.splice(myQueue.indexOf(userId), 1);
            io.sockets.in('wait').emit('UPDATE_QUEUE', myQueue);
        });

        //user kham benh
        socket.on('KHAM_BENH', (data) => {
                // socket.join('benhnhan');
                if (doctorOnlines.hasOnline()) {
                    var doctor = doctorOnlines.getOne(data.special) || doctorOnlines.getFisrt();
                    io.to(doctor.socketId).emit('DOCTOR_NOT_READY');
                    // console.log('log: ', doctor.socketId);
                    callToUser(data.userId, doctor.id, 1);
                } else {
                    socket.join('wait');
                    let found = myQueue.find(element => element === data.userId);

                    if (found === undefined) {
                        myQueue.push(data.userId);
                    }

                    // sep hang
                    io.sockets.emit(data.userId + '2', {
                        userIndex: myQueue.indexOf(data.userId) + 1,
                        rank: data.rank
                    });
                }
            }
        );

        socket.on('SOS', (userId) => {
            // socket.join('benhnhan');
            if (doctorOnlines.hasOnline()) {
                var doctor = doctorOnlines.getFisrt();
                io.to(doctor.socketId).emit('DOCTOR_NOT_READY');

                callToUser(userId, doctor.id, 2, true);
            } else {
                soss.push(userId);
            }
        });

        socket.on('DONE', data => {
            var idOfMethodKham = data.userId + data.doctorID;
            if (onKhams.has(idOfMethodKham)) {

                var method = onKhams.get(idOfMethodKham).method;

                io.to(socket.id).emit('FINISH', method);


            }

            User.findById(data.userId, function (err, list) {
                var newBalance = list.info.balance - 2;
                User.findByIdAndUpdate(data.userId, {"info.balance": newBalance}, {new: true}, function (err) {
                    if (err) {
                        console.log(err)
                    }
                });
            });
            User.findById(data.doctorID, function (err, list) {
                var newBalance = (Math.round((list.info.balance + 1.8) * 10)) / 10;
                User.findByIdAndUpdate(data.doctorID, {"info.balance": newBalance}, {new: true}, function (err) {
                    if (err) {
                        console.log(err)
                    }
                });
            });
            var money = new Money({
                money: 2,
            });
            money.user = new mongoose.mongo.ObjectId(data.userId);
            money.doctor = new mongoose.mongo.ObjectId(data.doctorID);
            money.save();
            io.sockets.emit(data.userId + 'done', data);
        });
    });

    var onKhams = new Map();


    function callToUser(myId, doctorId, method = 1, isSos = false) {
        onKhams.set(myId + doctorId, {
            method: method
        });

        getDoctorById(doctorId).then(function (doctor) {
            io.sockets.emit(myId, {
                myId: myId,
                friendId: doctorId,
                doctor: {
                    name: doctor.info.firstname + ' ' + doctor.info.lastname,
                    img: doctor.info.img,
                    specialties: doctor.doctor.specialties,
                    currentRating: doctor.doctor.currentRating
                },
                isSos: isSos
            })
        });
    }

    function getSocketIdByUserId(userId, listSocket) {
        for (let socketId in listSocket) {
            if (listSocket[socketId].userId === userId) {
                return socketId;
            }
        }
    }

};
