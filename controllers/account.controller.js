const passport = require('passport');
const userModel = require('../models/user.model');
require('mongoose-pagination');
const specialtiesController = require('../models/diagnosis.specialties.model');

// account register //admin + doctor
    // generate page
exports.generateAccountAdminRegister = (req, res) => {
    let messages = req.flash('error');
    dataForm = {
        specialties: '',
        desciption: '',
        firstname: '',
        lastname: '',
        email: '',
        numbercmnd: '',
        address: '',
        gender: '',
        img: '',
        dob: '',
        imgcmnn: '',
        password: ''
    };
    res.render('admin/register.ejs', {
        messages: messages,
        hasErrors: messages.length > 0,
        dataForm: dataForm,
        user: req.user
    });
};

    //checkInput Admin account
exports.checkInputAccountAdminRegister = (req, res, next) => {
    // form values
    //check form validation
    req.checkBody("firstname", "FirstName is required").notEmpty();
    req.checkBody("lastname", "LastName is required").notEmpty();
    req.checkBody("email", "Email is invalid").isEmail();
    req.checkBody("password", "Password is required").notEmpty();
    req.checkBody("password", 'Confirm password and password are not the same, please check again.').equals(req.body.password_confirmation);
    //check for errors
    let errors = req.validationErrors();
    dataForm = req.body;
    let messages = [];
    if (errors) {
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        res.render('admin/register.ejs', {
            messages: messages,
            hasErrors: messages.length > 0,
            dataForm: dataForm,
            user: req.user
        });
    } else {
        userModel.findOne({'local.email': req.body.email}, function (err, user) {
            if (err) {
                messages.push('Error saving account, please try again');
                res.render('admin/register.ejs', {
                    messages: messages,
                    hasErrors: messages.length > 0,
                    dataForm: dataForm,
                    user: req.user
                });
            }
            if (user) {
                messages.push('Email has been used, please choose another email!');
                res.render('admin/register.ejs', {
                    messages: messages,
                    hasErrors: messages.length > 0,
                    dataForm: dataForm,
                    user: req.user
                });
            } else {
                next();
            }
        });
    }
};

    //checkInput account doctor
exports.checkInputAccountDoctorRegister = (req, res, next) => {
    // form values
    //check form validation
    req.checkBody("specialties", "Specialties is required").notEmpty();
    req.checkBody("desciption", "Description is required").notEmpty();
    req.checkBody("firstname", "FirstName is required").notEmpty();
    req.checkBody("lastname", "LastName is required").notEmpty();
    req.checkBody("email", "Email is invalid").isEmail();
    req.checkBody("password", "Password is required").notEmpty();
    req.checkBody("password", 'Confirm password and password are not the same, please check again.').equals(req.body.password_confirmation);
    //check for errors
    let errors = req.validationErrors();
    dataForm = req.body;
    if (errors) {
        let messages = [];
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        res.render('admin/doctorCre.ejs', {
            messages: messages,
            hasErrors: messages.length > 0,
            dataForm: dataForm,
            user: req.user
        });
    } else {
        userModel.findOne({'local.email': req.body.email}, function (err, user) {
            if (err) {
                messages.push('Error saving account, please try again');
                res.render('admin/doctorCre.ejs', {
                    messages: messages,
                    hasErrors: messages.length > 0,
                    dataForm: dataForm,
                    user: req.user
                });
            }
            if (user) {
                messages.push('Email has been used, please choose another email!');
                res.render('admin/register.ejs', {
                    messages: messages,
                    hasErrors: messages.length > 0,
                    dataForm: dataForm,
                    user: req.user
                });
            } else {
                next();
            }
        });
    }
};

    //save account //admin account + doctor + user
exports.processAccountCreate = (req, res, next) => {
    let newUser = new userModel();
    newUser.info.firstname = req.body.firstname;
    newUser.info.lastname = req.body.lastname;
    newUser.info.lastname = req.body.lastname;
    newUser.info.numbercmnd = req.body.numbercmnd;
    newUser.info.address = req.body.address;
    newUser.info.gender = req.body.gender;
    let img = req.body.img;
    if(img === ''){img = 'https://www.touchtaiwan.com/images/default.jpg'}
    newUser.info.img = img;
    newUser.info.dob = req.body.dob;
    newUser.local.email = req.body.email;
    newUser.local.password = newUser.encryptPassword(req.body.password);
    let role = req.body.role;
    if (role.localeCompare("doctor") === 0) {
        let specs = req.body.specialties.split('@');
        newUser.doctor.specialtiesId = specs[0];
        newUser.doctor.specialties = specs[1];
        newUser.doctor.desciption = req.body.desciption;
        newUser.isDoctor = true;
    }
    if (role.localeCompare("admin") === 0) {
        newUser.isAdmin = true;
    }
    newUser.save(function (err, result) {
        if (err) {
            // return done(err);
            res.send('err');
        } else {
            if (role.localeCompare("doctor") === 0) {
                res.send('ok');
            } else if (role.localeCompare("admin") === 0) {
                let messages = ['Register success. Please login to continue'];
                res.render('admin/login.ejs', {
                    messages: messages,
                    hasErrors: messages.length > 0,
                    user: req.user
                });
            }
        }
    });
};

// account admin login
exports.generateAccountAdminLogin = function(req, res){
    let messages = req.flash('error');
    res.render('admin/login.ejs', {
        messages: messages,
        hasErrors: messages.length > 0,
        user: req.user
    });
};

// account admin login
exports.processAccountAdminLogin = passport.authenticate('local.login', {
        successRedirect: '/admin',
        failureRedirect: '/login/admin',
        failureFlash: true
    });

// account doctor
exports.generateAccountDoctorRegister = (req, res) => {
    let messages = req.flash('error');
    let spec = specialtiesController.getAll();
    dataForm = {
        specialties: '',
        desciption: '',
        firstname: '',
        lastname: '',
        email: '',
        numbercmnd: '',
        address: '',
        gender: '',
        img: '',
        dob: '',
        imgcmnn: '',
        password: ''
    };
    res.render('admin/doctorCre.ejs', {
        messages: messages,
        hasErrors: messages.length > 0,
        dataForm: dataForm,
        user: req.user,
        spec: spec,
    });
};

// get list account
exports.generateAccountList = (req, res)=>{
    let typeAccount = req.query.type || '';
    let headerPage;
    let statusAccount = req.query.status || 1;
    let option;
    if(typeAccount.localeCompare("admin") === 0){
        option = {isAdmin: true, 'status': statusAccount};
        headerPage = 'List of admin';
    }
    if(typeAccount.localeCompare("doctor") === 0){
        option = {isDoctor: true, 'status': statusAccount};
        headerPage = 'List of doctors';
    }
    if(typeAccount.localeCompare("user") === 0){
        option = {isDoctor: false, isAdmin: false, 'status': statusAccount};
        headerPage = 'List of users';
    }
    let page = req.query.page || 1;
    let limit = req.query.limit || 10;
    userModel.find(option).paginate(parseInt(page), parseInt(limit), function(err, list, total) {
        if(err){
            //handle err
            res.send(err)
        }
        // paginate info
        let statusPrevious = "";
        let statusNext = "";
        let totalPage = Math.ceil(total/limit);
        let endRecord = (page -1)*limit + parseInt(limit);
        let startRecord = (page - 1)*limit + 1;
        let previousPage = 1;
        let nextPage = totalPage;
        let endPage = totalPage;
        let startPage = 1;

        if(page > 1){ previousPage = page - 1;}
        if(page < totalPage){ nextPage = parseInt(page) + 1;}
        if(page === 1){ statusPrevious = "disabled";}
        if (page === totalPage){
            statusNext = "disabled";
            endRecord = total;
        }
        if(page >= 3){startPage = page - 2;}
        if(page <= totalPage - 2){endPage = parseInt(page) + 2;}
        if(total === 0){
            endRecord = 0;
            startRecord = 0;
        }
        let paginateIfo = {
            statusPrevious: statusPrevious,
            statusNext: statusNext,
            endRecord: endRecord,
            startRecord: startRecord,
            previousPage: previousPage,
            nextPage: nextPage,
            endPage: endPage,
            startPage: startPage,
        };
        //end paginate info
        let resData = {
            list: list,
            total: total,
            // 'totalPage': Math.ceil(total/limit),
            totalPage: totalPage,
            page: page,
            limit: limit,
            headerPage: headerPage,
            paginateIfo: paginateIfo,
            'user': req.user
        };
        res.render('admin/accountList', resData);
    })
};

// get detail account
exports.generateAccountDetail = function (req, res) {
    let messages = req.flash('error');
    dataForm = {
        specialties: '',
        desciption: '',
        firstname: '',
        lastname: '',
        email: '',
        numbercmnd: '',
        address: '',
        gender: '',
        img: '',
        dob: '',
        imgcmnn: '',
        password: ''
    };
    userModel.findById(req.params.id, function (err, obj) {
        if (err) {
            return res.status(500).send(err);
        } else {
            let resData = {
                obj: obj,
                header: 'Detail Account',
                messages: messages,
                hasErrors: messages.length > 0,
                dataForm: req.body,
                user: req.user
            };
            res.render('admin/accountDetail', resData);
        }
    });
};
