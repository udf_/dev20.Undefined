var Booking = require("../models/booking.model");
var Time = require("../models/time.model");
var User = require("../models/user.model");
var DoctorTime = require("../models/doctorTime.model");
var mongoose = require('mongoose');
var myid = mongoose.Types.ObjectId;


exports.listTimeAdmin = function (req, res) {
    Time.find({}, function (err, list) {
        res.render("admin/time.ejs", {
            "listTime": list,
            'user': req.user
        });
    });
};

exports.listBook = function (req, res) {
    User.find({isDoctor: true}, function (err, docs1) {
        Time.find({}, function (err, docs2) {
            res.render('client/booking.ejs', {
                user: req.user,
                "listDoctor": docs1,
                "listTime": docs2,
            });
        });
    });
};

exports.listTimeBook = function (req, res) {
    var ranks = req.query.rank || '';
    var rank = ranks.split(',').reverse();
    User.find({isDoctor: true}, function (err, docs1) {
        docs1.sort(function (a, b) {
            var sIdA = a.doctor.specialtiesId.toString();
            var sIdB = b.doctor.specialtiesId.toString();
            var rankOfDA = rank.indexOf(sIdA);
            var rankOfDB = rank.indexOf(sIdB);
            var starA = a.doctor.currentRating;
            var starB = b.doctor.currentRating;


            if (rankOfDA === rankOfDB) {
                return starA > starB ? -1 : 1;
            } else if (rankOfDA < rankOfDB) {
                return 1;
            } else return -1;
        });
        var responseData = {
            user: req.user,
            'doctors': docs1
        };

        res.render('client/booking.ejs', responseData);
    });
};

exports.createTime = function (req, res) {
    var time = new Time({
        time: req.body.time,
        index: req.body.index
    });
    time.save();
    res.redirect(req.get('referer'));
};

