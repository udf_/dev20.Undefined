var User = require("../models/user.model");
var Rate = require("../models/rate.model");
var mongoose = require('mongoose');


exports.sendRate = function (req, res) {
    if (req.body.rate) {
        var rate = new Rate({
            rate: req.body.rate,
        });
        rate.user = new mongoose.mongo.ObjectId(req.body.userId);
        rate.doctor = new mongoose.mongo.ObjectId(req.body.doctorId);
        rate.save();
        var current = 0;
        var totalRate = 0;
        var doctorId = req.body.doctorId;
        console.log(req.body.doctorId);
        Rate.find({})
            .where('doctor').eq(new mongoose.mongo.ObjectId(doctorId))
            .exec(function (err, list) {
                for (let i = 0; i < list.length; i++) {
                    totalRate += list[i]["rate"];
                }
                current = Math.round(totalRate / list.length / 5 * 100);
                User.findByIdAndUpdate( doctorId, {"doctor.currentRating": current}, {new: true}, function (err) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.redirect('/');
                    }
                });
            });
    }else {
        res.redirect('/');
    }

};




