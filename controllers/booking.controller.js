var Booking = require("../models/booking.model");
var Time = require("../models/time.model");
var DoctorTime = require("../models/doctorTime.model");
var User = require("../models/user.model");
var mongoose = require('mongoose');
var myid = mongoose.Types.ObjectId;
var sendMail = require('./sendMail.controller');

exports.sendBooking = function (req, res) {
    var timeDoc = req.body.timeDoc.split('-');
    var booking = new Booking({
        time: timeDoc[0]
    });

    booking.user = new mongoose.mongo.ObjectId(req.body.user);
    booking.doctor = new mongoose.mongo.ObjectId(req.body.doctor);
    DoctorTime.findByIdAndUpdate(timeDoc[1], {status: "1"}, {new: true}, function (err) {
        if (err) {
            res.send(err);
        } else {
            console.log(booking.time);

            booking.save();
            User.findById(req.body.user, function (err, user) {
                var newBalance = user.info.balance - 10;
                User.findByIdAndUpdate(req.body.user, {"info.balance": newBalance}, {new: true}, function (err) {
                    if (err) {
                        res.send(err);
                    } else {
                        sendMail.sendMailBookingDone(user.local.email, booking.time);
                    }
                });
            });
            User.findById(req.body.doctor, function (err, doctor) {
                var newBalance = doctor.info.balance + 9;
                var nameDoctor = doctor.info.firstname +' '+ doctor.info.lastname;
                User.findByIdAndUpdate(req.body.doctor, {"info.balance": newBalance}, {new: true}, function (err) {
                    if (err) {
                        res.send(err);
                    } else {
                        sendMail.sendMailHasBooking(doctor.local.email, booking.time);
                    }
                });

            });

            res.redirect('/')
        }
    });
};

exports.listBooking = function (req, res) {
    Booking.find({}, function (err, list) {
        res.render("admin/booking.ejs", {
            "listBooking": list,
            'user': req.user
        });
    });

};

exports.deleteRegister = function (req, res) {
    Booking.findByIdAndRemove(myid(req.params.id), function (err) {
        if (err)
            res.send(err);
        else
            res.redirect(req.get('referer'));
    });
};

exports.updateRegister = function (req, res) {
    Booking.findByIdAndUpdate(req.params.id, req.body, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.redirect(req.get('referer'));
        }
    });
};


