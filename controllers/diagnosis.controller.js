const locationsController = require('../models/diagnosis.locations.model');
const symptomsController = require('../models/diagnosis.symptoms.model');
const diseaseController = '';
const specialtiesController = require('../models/diagnosis.specialties.model');
const drugController = require('../models/drug.model');
var User = require("../models/user.model");

exports.generateSuggest = function (req, res) {
    res.render('client/suggest.ejs', {user: req.user});
};

exports.generateSuggestLocations = function (req, res) {
    let list = locationsController.getAll();
    res.render('client/suggestLocations.ejs', {list: list});

};

exports.processApiLocations = function (req, res) {
    let list = [];
    let location = locationsController.getOneById(req.query.locationId);
    let symId = location.symptomsId;
    for (let j = 0; j < symId.length; j++) {
        list.push(symptomsController.getById(symId[j]));
    }
    res.json(list)
};

exports.processApiResult = function (req, res) {
    let listId = [];
    let listSpecial = [];
    let listIdSymptoms = req.body.data.split(',');
    listIdSymptoms.forEach(function (idSymptom) {
        let symptom = symptomsController.getById(idSymptom);
        let specId = symptom.specialtiesId;
        for (let j = 0; j < specId.length; j++) {
            let id = specId[j];
            if (listId.indexOf(id) === -1) {
                listId.push(id);
                let special = specialtiesController.getById(id);
                special.count = 1;
                listSpecial.push(special);
            } else {
                let special = specialtiesController.getById(id);
                let index = listSpecial.indexOf(special);
                let special1 = listSpecial[index];
                special1.count += 1;
                listSpecial[index] = special1;
            }
        }
    });
    listSpecial.sort((a, b) => {
        return b.count - a.count;
    });
    res.json(listSpecial);
};

exports.processApiSymptomSearch = function (req, res) {
    let q = req.query.q.toLowerCase();
    let respondData = [];
    let list = symptomsController.getAll();
    list.forEach(function (smp) {
        if (smp.name.toLowerCase().search(q) !== -1) {
            respondData.push(smp)
        }
    });
    res.json(respondData)
};

exports.processApiDiseaseSearch = function (req, res) {
    let q = req.query.q.toLowerCase();
    let respondData = [];
    let list = diseaseController.getAll();
    list.forEach(function (smp) {
        if (smp.name.toLowerCase().search(q) !== -1) {
            respondData.push(smp)
        }
    });
    res.json(respondData)
};

exports.processApiDrugSearch = function (req, res) {
    let q = req.query.q.toLowerCase();
    let respondData = [];
    let list = drugController.getAll();
    list.forEach(function (smp) {
        if (smp.name.toLowerCase().search(q) !== -1) {
            respondData.push(smp)
        }
    });
    res.json(respondData)
};



