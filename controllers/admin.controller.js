const userModel = require('../models/user.model');
const moneyModel = require('../models/money.model');

exports.generateHome = async function (req, res) {
    let data = {};
    let year = new Date().getFullYear();
    let month = new Date().getMonth();
    let day = new Date().getDate();
    let startDay = new Date(year, month);
    let endDay = new Date();
    let currentDay = new Date(year, month, day);
    let currentYear = new Date(year);
    let optionDoctorMonth = {'info.createdAt': {$gte: startDay, $lt: endDay}, 'isDoctor': true, 'status': 1};
    let optionUserMonth = {'info.createdAt': {$gte: startDay, $lt: endDay}, 'isDoctor': false, 'status': 1};
    let optionDoctorAll = {'isDoctor': true, 'status': 1};
    let optionUserAll = {'isDoctor': false, 'status': 1};
    let optionMoneyMonth = {'createAt': {$gte: startDay, $lt: endDay}};
    let optionMoneyDay = {'createAt': {$gte: currentDay}};
    let optionMoneyYear = {'createAt': {$gte: currentYear}};

    await userModel.find(optionDoctorMonth, function (err, listDoctorMonth) {
        data.newDoctorMonthly = listDoctorMonth.length;
    });
    await userModel.find(optionUserMonth, function (err, listUserMonth) {
        data.newUserMonthly = listUserMonth.length;
    });
    await userModel.find(optionDoctorAll, function (err, listDoctorAll) {
        data.doctorAll = listDoctorAll.length;
    });
    await userModel.find(optionUserAll, function (err, listUserAll) {
        data.userAll = listUserAll.length;
    });

    await moneyModel.find(optionMoneyMonth, function (err, list) {
        let moneyMonth = 0;
        data.callMonthly = list.length;
        if (data.callMonthly !== 0) {
            list.forEach(function (l) {
                moneyMonth += l.money;
            });
            data.moneyUserMonth = moneyMonth;
            data.webEarningsMonthly = Math.round(moneyMonth) / 10;
            data.earningsDoctorMonthly = moneyMonth - data.webEarningsMonthly;
        } else {
            data.moneyUserMonth = 0;
            data.webEarningsMonthly = 0;
            data.earningsDoctorMonthly = 0;
        }
    });
    await moneyModel.find(optionMoneyDay, function (err, list) {
        let moneyDay = 0;
        if (list.length !== 0) {
            list.forEach(function (l) {
                moneyDay += l.money;
            });
            data.moneyUserDay = moneyDay;
            data.webEarningsDay = Math.round(moneyDay) / 10;
            data.earningsDoctorDay = moneyDay - data.webEarningsDay;
        } else {
            data.moneyUserDay = 0;
            data.webEarningsDay = 0;
            data.earningsDoctorDay = 0;
        }
    });
    await moneyModel.find(optionMoneyYear, function (err, list) {
        let earnings = [];
        if (list.length !== 0) {
            list.forEach(function (l) {
                let month = new Date(l.createAt).getMonth();
                if (earnings[month] === undefined) {
                    earnings[month] = l.money;
                } else {
                    earnings[month] += l.money;
                }
            });
        } else {
            data.earningsWeb = [0]
        }
        let earningsLength = earnings.length;
        for (let i = 0; i < earningsLength; i++) {
            if (earnings[i] === undefined) {
                earnings[i] = 0;
            } else {
                earnings[i] = Math.round(earnings[i])/10;
            }
        }
        data.earningsWeb = earnings;
    });
    await res.render('admin/home', {'data': data, 'user': req.user});
};
