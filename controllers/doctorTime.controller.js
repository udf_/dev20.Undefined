var Booking = require("../models/booking.model");
var Time = require("../models/time.model");
var DoctorTime = require("../models/doctorTime.model");
var mongoose = require('mongoose');
var User = require("../models/user.model");
var moment = require('moment');
var myid = mongoose.Types.ObjectId;

exports.createDoctorTime = function (req, res) {
    console.log('date: ', req.body.date);
    console.log(typeof req.body.date);
    Time.find({}, function (err, list) {
        list.forEach(function (element) {
            var time = moment(req.body.date +' '+ element.time.slice(0, 5), 'YYYY-MM-DD hh:mm').add(-7,'h').valueOf();
            var obj = new DoctorTime({
                date: req.body.date,
                status: '0'
            });
            obj.doctor = new mongoose.mongo.ObjectId(req.user.id);
            obj.time = time;
            obj.save();
        });
    });
    res.redirect('/');
};

exports.listDoctorTime = function (req, res) {
    DoctorTime.find({}, function (err, list) {
        res.render("admin/doctorTime.ejs", {
            "listDoctorTime": list
        });
    });
};

exports.doctorTimeBook = function (req, res) {
    DoctorTime.find()
        .where('doctor').eq(new mongoose.mongo.ObjectId(req.params.id))
        .exec(function (err, list) {

            res.json(list)
        })
};
