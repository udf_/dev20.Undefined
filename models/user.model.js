var mongoose = require('mongoose');
const bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    doctor: {
        specialtiesId: Number,
        specialties: String,
        desciption: String,
        doctorTime: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'TimeDoctor'
        }],
        rate: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Rate'
        }],
        currentRating: {
            type: Number,
            default: 0
        },
    },
    info: {
        firstname: String,
        lastname: String,
        img: String,
        numbercmnd: String,
        address: String,
        gender: String,
        dob: Date,
        imgcmnn: String,
        createdAt: {type: Date, default: Date.now},
        updateAt: {
            type: Date,
            default: null
        },
        deleteAt: {
            type: Date,
            default: null
        },
        balance: {
            type: Number,
            default: null
        }

    },
    local: {
        email: {
            type: String
        },
        password: {
            type: String
        }
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isDoctor: {
        type: Boolean,
        default: false
    },
    status: {
        type: Number,
        default: 1
    }
});

userSchema.methods.encryptPassword = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);
