// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;
//
// let drugSchema = new Schema({
//     drugName: {type: String},
//     createdAt: {type: Date, default: Date.now},
//     updateAt: {type: Date, default: null},
//     deleteAt: {type: Date, default: null},
//     status: {type: Number, default: 1},
// });
//
// module.exports = mongoose.model("Drug", drugSchema);

let drugData = [
    {'_id': 0, 'name': 'Corypadol', 'description': 'Corypadol'},
    {'_id': 1, 'name': 'ClanzaCR', 'description': 'ClanzaCR'},
    {'_id': 2, 'name': 'Paracetamol 500mg', 'description': 'Paracetamol 500mg'},
    {'_id': 3, 'name': 'Paracetamol BP 500mg', 'description': 'Paracetamol BP 500mg'},
    {'_id': 4, 'name': 'Dopagan 500mg', 'description': 'Dopagan 500mg'},
    {'_id': 5, 'name': 'Dopagan Effervescent 500mg', 'description': 'Dopagan Effervescent 500mg'},
];

module.exports = drugData;

let drugController = {
    getAll: function () {
        return drugData;
    },
    getOne: function (req, res) {
        let id = req.params.id - 1;
        return drugData[id];
    },
    getOneById: function (id) {
        return drugData[id - 1];
    },
};
module.exports = drugController;
