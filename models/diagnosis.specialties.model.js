dataSpecialties = [
        {
            "_id": 1,
            "name": "General examination department",
            "description": "Department of Examination (Khoa Khám bệnh tổng hợp)",
            "sex": "all",
            "img": "",
            "createAt": "2019-06-04",
            "status": 1
        },
        {
            "_id": 2,
            "name": "Neurology",
            "description": "Neurology (Khoa thần kinh)",
            "sex": "all",
            "img": "",
            "createAt": "2019-05-29",
            "updateAt": "",
            "deleteAt": "",
            "status": 1
        },
        {
            "_id": 3,
            "name": "Gastroenterology",
            "description": "Gastroenterology (Khoa nội tiêu hóa)",
            "sex": "all",
            "img": "",
            "createAt": "2019-05-29",
            "updateAt": "",
            "deleteAt": "",
            "status": 1
        },
        {
            "_id": 4,
            "name": "Cardiology",
            "description": "Cardiology (Khoa Nội Tim mạch)",
            "sex": "all",
            "img": "",
            "createAt": "2019-05-29",
            "updateAt": "",
            "deleteAt": "",
            "status": 1
        },
        {
            "_id": 5,
            "name": "Hepato-Biliary-Pancreatic ",
            "description": "Hepato-Biliary-Pancreatic  (Khoa Gan – Mật – Tụy)",
            "sex": "all",
            "img": "",
            "createAt": "2019-05-29",
            "updateAt": "",
            "deleteAt": "",
            "status": 1
        },
        {
            "_id": 6,
            "name": "Ophthalmology",
            "description": "Ophthalmology (Khoa mắt)",
            "sex": "all",
            "img": "",
            "createAt": "2019-05-29",
            "updateAt": "",
            "deleteAt": "",
            "status": 1
        },
        {
            "_id": 7,
            "name": "Ear – Nose -Throat",
            "description": "Ear – Nose -Throat (Khoa tai-mũi-họng)",
            "sex": "all",
            "img": "",
            "createAt": "2019-06-01",
            "status": 1
        },
        {
            "_id": 8,
            "name": "Respiratory medicine",
            "description": "Respiratory medicine (Khoa Hô Hấp)",
            "sex": "all",
            "img": "",
            "createAt": "2019-06-04",
            "status": 1
        },
        {
            "_id": 9,
            "name": "General internal medicine",
            "description": "General internal medicine (Khoa nội tổng hợp)",
            "sex": "all",
            "img": "",
            "createAt": "2019-05-29",
            "updateAt": "2019-06-01",
            "deleteAt": "",
            "status": 1
        }
    ]

;
module.exports = dataSpecialties;

let controllerSpecialties = {
    getAll: function () {
        return dataSpecialties;
    },
    getOne: function (req, res) {
        let id = req.params.id - 1;
        return dataSpecialties[id];
    },
    getById: function (id) {
        return dataSpecialties[id - 1];
    },
};
module.exports = controllerSpecialties;
