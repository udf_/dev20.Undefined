const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var timeDoctorSchema = new Schema({
    time: Number,
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    date: {type: String},
    status: {type: String},
    booking:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Booking'
    }]
});

module.exports = mongoose.model("TimeDoctor", timeDoctorSchema);
