const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var recordSchema = new Schema({
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    symptom: {type: String},
    diagnosis: {type: String},
    medicine: {type: String},
    amount: {type: String},
    using: {type: String},
    createAt: {
        type: String,
        default: Date.now()
    },
    updateAt: {type: String},
    deleteAt: {type: String},
    status: {type: Number}
});

module.exports = mongoose.model("Record", recordSchema);


