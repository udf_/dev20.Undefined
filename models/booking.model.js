const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var bookingSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    time: Number,
    createAt: {
        type: String,
        default: Date.now()
    },
    updateAt: {
        type: String,
        default: null
    },
    deleteAt: {
        type: String,
        default: null
    },
    status: {
        type: Number,
        default:1
    }
});

module.exports = mongoose.model("Booking", bookingSchema);
