const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var moneySchema = new Schema({
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    money: Number,
    createAt: {
        type: Number,
        default: Date.now()
    }
});

module.exports = mongoose.model("Money", moneySchema);
