dataSymptoms = [
    {
        "_id": 1,
        "name": "Headache",
        "description": "Headache (Đau đầu)",
        "specialtiesId": [2, 4],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 2,
        "name": "Dizziness",
        "description": "Dizziness (Chóng mặt)",
        "specialtiesId": [2, 4],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 3,
        "name": "Hair loss",
        "description": "Hair loss (Rụng tóc)",
        "specialtiesId": [2],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 4,
        "name": "Facial pain",
        "description": "Facial pain (Đau mặt)",
        "specialtiesId": [2],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 5,
        "name": "Face swelling",
        "description": "Face swelling (Mặt sưng)",
        "specialtiesId": [2],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 6,
        "name": "Swollen cheek",
        "description": "Swollen cheek (Má sưng)",
        "specialtiesId": [2],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 7,
        "name": "Vesicles filled with pus, up to 1 cm in diameter",
        "description": "Vesicles filled with pus, up to 1 cm in diameter (Mụn nước chứa đầy mủ, đường kính lên tới 1 cm)",
        "specialtiesId": [2],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 8,
        "name": "Fainting",
        "description": "Fainting (Ngất xỉu)",
        "specialtiesId": [2, 4],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 9,
        "name": "Impaired memory",
        "description": "Impaired memory (Trí nhớ kém)",
        "specialtiesId": [2],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 10,
        "name": "Feeling sick",
        "description": "Feeling sick (Cảm thấy bệnh)",
        "specialtiesId": [1, 2, 4, 5],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 11,
        "name": "Passing out, loss of consciousness",
        "description": "Passing out, loss of consciousness (Bất tỉnh, mất ý thức)",
        "specialtiesId": [2, 4],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 12,
        "name": "Consciousness disturbances",
        "description": "Consciousness disturbances (Rối loạn ý thức)",
        "specialtiesId": [2],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 13,
        "name": "Eye pain",
        "description": "Eye pain (Đau mắt)",
        "specialtiesId": [6],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 14,
        "name": "Red eye",
        "description": "Red eye (Mắt đỏ)",
        "specialtiesId": [6],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 15,
        "name": "Red and stinging eyes",
        "description": "Red and stinging eyes (Mắt đỏ và cay)",
        "specialtiesId": [6],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 16,
        "name": "Itching of eyes",
        "description": "Itching of eyes (Ngứa mắt)",
        "specialtiesId": [6],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 17,
        "name": "Watery eyes",
        "description": "Watery eyes (Chảy nước mắt)",
        "specialtiesId": [6],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 18,
        "name": "Yellow eyes",
        "description": "Yellow eyes (Đôi mắt màu vàng)",
        "specialtiesId": [6],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 19,
        "name": "Pain near eye socket",
        "description": "Pain near eye socket (Đau gần hốc mắt)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 20,
        "name": "Impaired vision",
        "description": "Impaired vision (Suy giảm thị lực)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 21,
        "name": "Seeing double",
        "description": "Seeing double (Thấy cả hai)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 22,
        "name": "Temporary blindness",
        "description": "Temporary blindness (Mù tạm thời)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 23,
        "name": "Nystagmus",
        "description": "Nystagmus (Chứng giật nhãn cầu)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 24,
        "name": "Asymmetric pupils",
        "description": "Asymmetric pupils (Học sinh bất đối xứng)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 25,
        "name": "Eyelid twitching",
        "description": "Eyelid twitching (Mí mắt co giật)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 26,
        "name": "Skin change on eyelid",
        "description": "Skin change on eyelid (Thay đổi da trên mí mắt)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 27,
        "name": "Trembling eyelids",
        "description": "Trembling eyelids (Mí mắt run rẩy)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 28,
        "name": "Drooping eyelids",
        "description": "Drooping eyelids (Mí mắt rơi)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 29,
        "name": "Dry discharge on eyelids",
        "description": "Dry discharge on eyelids (Xả khô trên mí mắt)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 30,
        "name": "Eye flashes",
        "description": "Eye flashes (Mắt lóe sáng)",
        "specialtiesId": [
            6
        ],
        "createAt": "2019-05-29",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 31,
        "name": "Earache",
        "description": "Earache (Đau tai)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 32,
        "name": "Decreased hearing",
        "description": "Decreased hearing (Giảm thính lực)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 33,
        "name": "Discharge from ear",
        "description": "Discharge from ear (Xả tai)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 34,
        "name": "Clogged ear",
        "description": "Clogged ear (Tai bị tắc)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 35,
        "name": "Itching in ear",
        "description": "Itching in ear (Ngứa trong tai)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 36,
        "name": "Ringing in ears",
        "description": "Ringing in ears (Đổ chuông)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 37,
        "name": "Runny nose",
        "description": "Runny nose (Sổ mũi)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 38,
        "name": "Blocked nose",
        "description": "Blocked nose (Nghẹt mũi)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 39,
        "name": "Itching of nose or throat",
        "description": "Itching of nose or throat (Ngứa mũi hoặc cổ họng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 40,
        "name": "Impaired smell",
        "description": "Impaired smell (Mùi khó chịu)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 41,
        "name": "Bleeding from nose",
        "description": "Bleeding from nose (Chảy máu mũi)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 42,
        "name": "Nasal voice",
        "description": "Nasal voice (Giọng mũi)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 43,
        "name": "Mucus dripping at the back of the throat",
        "description": "Mucus dripping at the back of the throat (Chất nhầy nhỏ giọt ở phía sau cổ họng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 44,
        "name": "Sneeze",
        "description": "Sneeze (Hắt hơi)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 45,
        "name": "Snoring",
        "description": "Snoring (Ngáy)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 46,
        "name": "Frequent infections",
        "description": "Frequent infections (Nhiễm trùng thường xuyên)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 47,
        "name": "Shortness of breath",
        "description": "Shortness of breath (Khó thở)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 48,
        "name": "Toothache",
        "description": "Toothache (Bệnh đau răng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 49,
        "name": "Loose teeth",
        "description": "Loose teeth (Răng lung lay)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 50,
        "name": "Gum pain",
        "description": "Gum pain (Đau nướu răng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 51,
        "name": "Swollen gums",
        "description": "Swollen gums (Nướu sưng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 52,
        "name": "Bleeding gums",
        "description": "Bleeding gums (Nướu chảy máu)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 53,
        "name": "Unusually red tongue",
        "description": "Unusually red tongue (Lưỡi đỏ bất thường)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 54,
        "name": "Burning tongue",
        "description": "Burning tongue (Đốt lưỡi)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 55,
        "name": "Mouth ulcers",
        "description": "Mouth ulcers (Loét miệng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 56,
        "name": "Bad breath",
        "description": "Bad breath (Hôi miệng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 57,
        "name": "Dry mouth",
        "description": "Dry mouth (Khô miệng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 58,
        "name": "Difficulty biting and chewing",
        "description": "Difficulty biting and chewing (Khó cắn và nhai)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 59,
        "name": "Cough",
        "description": "Cough (Ho)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 60,
        "name": "Snoring",
        "description": "Snoring (Ngáy)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 61,
        "name": "Vomiting",
        "description": "Vomiting (Nôn)",
        "specialtiesId": [7],
        "createAt": "2019-06-01",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 62,
        "name": "Sore throat",
        "description": "Sore throat (Viêm họng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 63,
        "name": "Pain while swallowing",
        "description": "Pain while swallowing (Đau khi nuốt)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 64,
        "name": "Red throat",
        "description": "Red throat (Cổ họng đỏ)",
        "specialtiesId": [
            7,
            8
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 65,
        "name": "Hoarseness",
        "description": "Hoarseness (Khàn tiếng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 66,
        "name": "Shortness of breath",
        "description": "Shortness of breath (Khó thở)",
        "specialtiesId": [
            7,
            8
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 67,
        "name": "Cough",
        "description": "Cough (Ho)",
        "specialtiesId": [
            7,
            8
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 68,
        "name": "Difficulty swallowing",
        "description": "Difficulty swallowing (Khó nuốt)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 69,
        "name": "Enlarged lymph nodes",
        "description": "Enlarged lymph nodes (Hạch bạch huyết mở rộng)",
        "specialtiesId": [
            7
        ],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 70,
        "name": "Swollen neck",
        "description": "Swollen neck (Cổ sưng)",
        "specialtiesId": [7, 8],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 71,
        "name": "Palpable neck mass",
        "description": "Palpable neck mass (Khối cổ sờ thấy)",
        "specialtiesId": [7],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 72,
        "name": "Neck pain",
        "description": "Neck pain (Đau cổ)",
        "specialtiesId": [7],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 73,
        "name": "Stiff neck",
        "description": "Stiff neck (Cổ cứng)",
        "specialtiesId": [7],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 74,
        "name": "Chest pain",
        "description": "Chest pain (Đau ngực)",
        "specialtiesId": [8],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 75,
        "name": "Palpitations",
        "description": "Palpitations (Đánh trống ngực)",
        "specialtiesId": [1],
        "createAt": "2019-06-06",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 76,
        "name": "Fast heartbeat",
        "description": "Fast heartbeat (Tim đập nhanh)",
        "specialtiesId": [1],
        "createAt": "2019-06-06",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 77,
        "name": "Slow heart rate",
        "description": "Slow heart rate (Nhịp tim chậm)",
        "specialtiesId": [1],
        "createAt": "2019-06-06",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 78,
        "name": "Cough",
        "description": "Cough (Ho)",
        "specialtiesId": [7, 8],
        "createAt": "2019-06-06",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 79,
        "name": "Shortness of breath",
        "description": "Shortness of breath (Khó thở)",
        "specialtiesId": [7, 8],
        "createAt": "2019-06-06",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 80,
        "name": "Whistling sound made while breathing",
        "description": "Whistling sound made while breathing (Tiếng huýt sáo phát ra trong khi thở)",
        "specialtiesId": [8],
        "createAt": "2019-06-06",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 81,
        "name": "Heartburn",
        "description": "Heartburn (Chứng ợ nóng)",
        "specialtiesId": [3, 8],
        "createAt": "2019-06-06",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    }
];
module.exports = dataSymptoms;

let controllerSymptoms = {
    getAll: function () {
        return dataSymptoms;
    },
    getOne: function (req, res) {
        let id = req.params.id;
        return dataSymptoms[id] - 1;
    },
    getById: function (id) {
        return dataSymptoms[id - 1];
    },
};
module.exports = controllerSymptoms;
