const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var timeSchema = new Schema({
    time: {type: String},
    index: {type: Number},
    doctorTime:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TimeDoctor'
    }],

});

module.exports = mongoose.model("Time", timeSchema);
