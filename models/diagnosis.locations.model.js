dataLocations = [
    {
        "_id": 1,
        "name": "Head",
        "description": "Head (Đầu)",
        "symptomsId": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 2,
        "name": "Eyes",
        "description": "Eyes (Mắt)",
        "symptomsId": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 3,
        "name": "Ears",
        "description": "Ears (Tai)",
        "symptomsId": [31, 32, 33, 34, 35, 36],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 4,
        "name": "Nose",
        "description": "Nose (Mũi)",
        "symptomsId": [37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 5,
        "name": "Oral cavity",
        "description": "Oral cavity (Khoang miệng)",
        "symptomsId": [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 6,
        "name": "Neck or throat",
        "description": "Neck or throat (Cổ hoặc cổ họng)",
        "symptomsId": [62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73],
        "createAt": "2019-06-04",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 7,
        "name": "Chest",
        "description": "Chest (Ngực)",
        "symptomsId": [74, 75, 76, 77, 78, 79, 80, 81],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 8,
        "name": "Upper arm",
        "description": "Upper arm (Cánh tay trên)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 9,
        "name": "Elbow",
        "description": "Elbow (Khuỷu tay)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 10,
        "name": "Forearm",
        "description": "Forearm (Cánh tay)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 11,
        "name": "Hand",
        "description": "Hand (Bàn Tay)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 12,
        "name": "Breasts",
        "description": "Breasts (Vú)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 13,
        "name": "Upper abdomen",
        "description": "Upper abdomen (Bụng trên)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 14,
        "name": "Middle abdomen",
        "description": "Middle abdomen (Bụng giữa)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 15,
        "name": "Lower abdomen",
        "description": "Lower abdomen (Bụng dưới)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 16,
        "name": "Sexual organs",
        "description": "Sexual organs (Cơ quan sinh dục nữ)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 17,
        "name": "Sexual organs",
        "description": "Sexual organs (Cơ quan sinh dục nam)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 18,
        "name": "Thigh",
        "description": "Thigh (Đùi)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 19,
        "name": "Knee",
        "description": "Knee (Đầu gối)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 20,
        "name": "Lower leg",
        "description": "Lower leg (Cẳng chân)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 21,
        "name": "Foot",
        "description": "Foot (Bàn chân)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 22,
        "name": "Nape of neck",
        "description": "Nape of neck (Gáy cổ)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 23,
        "name": "Back",
        "description": "Back (Lưng)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 24,
        "name": "Lower back",
        "description": "Lower back (Thắt lưng)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 25,
        "name": "Buttocks",
        "description": "Buttocks (Mông)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    },
    {
        "_id": 26,
        "name": "Anus",
        "description": "Anus (Hậu môn)",
        "symptomsId": [],
        "createAt": "2019-05-30",
        "updateAt": "",
        "deleteAt": "",
        "status": 1
    }
];

module.exports = dataLocations;

let controllerLocations = {
    getAll: function () {
        return dataLocations;
    },
    getOne: function (req, res) {
        let id = req.params.id - 1;
        return dataLocations[id];
    },
    getOneById: function (id) {
        return dataLocations[id - 1];
    },
};
module.exports = controllerLocations;
