const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var rateSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    rate: Number,
});

module.exports = mongoose.model("Rate", rateSchema);
