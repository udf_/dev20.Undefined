const cloudName = 'dyi6c1dgi';
const unsignedUploadPreset = 'jfrzncgn';
const image = document.querySelector('[name="image"]');
image.onchange = function () {
    let file = this.files[0];
    let url = `https://api.cloudinary.com/v1_1/${cloudName}/upload`;
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let responseDataJson = JSON.parse(this.responseText);
            let imageUrl = document.querySelector('input[name="img"]');
            imageUrl.value = responseDataJson.url;
            document.getElementById('image-preview').src = imageUrl.value;
            document.getElementById('image-status').innerText = 'Upload image success';
        }
    };
    xhr.open('POST', url, true);
    let fd = new FormData();
    fd.append('upload_preset', unsignedUploadPreset);
    fd.append('tags', 'browser_upload');
    fd.append('file', file);
    xhr.send(fd);
};