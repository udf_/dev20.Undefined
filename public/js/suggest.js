$('.body-area').on('click', function () {
    $('.body-area').removeClass('body-area-selected');
    $(this).addClass('body-area-selected');
});

function load(locationId, locationName, cFunction) {
    document.getElementById("location-name").innerHTML = locationName;
    document.getElementById("symptoms-list").innerHTML = "<div><img src=\"/img/loadingxin.svg\" alt=\"\"></div>";
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            cFunction(this);
        }
    };
    let url = "/suggest/api/" + locationId + "?locationId=" + locationId;
    xhttp.open("GET", url, true);
    xhttp.send();
}

function locationsLoad(xhttp) {
    let symptoms = xhttp.responseText;
    let data = JSON.parse(symptoms);
    let html = '';
    data.forEach(function (symp) {
        html += "<div><label class='pointer'><input type='checkbox' name='symptoms' value='" + symp._id + "'>" + symp.name + "</label></div>";
    });
    document.getElementById("symptoms-list").innerHTML = html;
}

function selectWholeHead() {
    document.getElementById("location-name").innerHTML = "Whole head";
    let html = "";
    html += "<div onclick=\"load(1, 'Head', locationsLoad)\" class='pointer'>Head</div>";
    html += "<div onclick=\"load(2, 'Eyes',locationsLoad)\" class='pointer'>Eyes</div>";
    html += "<div onclick=\"load(3, 'Ears',locationsLoad)\" class='pointer'>Ears</div>";
    html += "<div onclick=\"load(4, 'Nose', locationsLoad)\" class='pointer'>Nose</div>";
    html += "<div onclick=\"load(5, 'Oral cavity', locationsLoad)\" class='pointer'>Oral cavity</div>";
    document.getElementById("symptoms-list").innerHTML = html;
}

function toggleBody() {
    let femaleFront = document.querySelectorAll(".model-body");
    femaleFront.forEach(function (ent) {
        if (ent.style.display === "none") {
            ent.style.display = "block";
        } else {
            ent.style.display = "none";
        }
    })
}

function selectSex(sex) {
    document.querySelector(".view-sex").style.display = "none";
    document.querySelector(".view-location").style.display = "block";
    if (sex === 'male') {
        document.getElementById("model-male").style.display = "block";
    } else {
        document.getElementById('model-female').style.display = "block";
    }
}

function back() {
    document.querySelector(".view-sex").style.display = "block";
    document.querySelector(".view-location").style.display = "none";
    document.getElementById("model-male").style.display = "none";
    document.getElementById('model-female').style.display = "none";
}

function submit() {
    let forms = document.forms['symptoms']['symptoms'];
    let listId = [];
    let formdata = new FormData();
    forms.forEach(function (form) {
        if (form.checked) {
            listId.push(form.value);
        }
    });

    formdata.append('data', listId);
    let dataSend = JSON.stringify(listId);
    let xhttp = new XMLHttpRequest();
    let searchParams = new URLSearchParams(window.location.search);
    var isBooking = searchParams.has('booking');
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let result = this.responseText;
            let data = JSON.parse(result);
            var newData = data.map(a => a._id);
            var dataForBooking = newData.join(',').toString();
            if (isBooking) {
                console.log(dataForBooking);
                window.location.replace('/booking?rank=' + dataForBooking);
            } else {
                socket.emit('KHAM_BENH', {
                    'userId': $('#myId').val(),
                    'special': newData,
                    'rank':dataForBooking
                });
            }
        }
    };

    let url = "/suggest/api/result";
    xhttp.open("POST", url, true);
    xhttp.send(formdata);
}
