const express = require('express');
var recordController = require('../controllers/record.controller');
var router = express.Router();

router.get('/form', function (req, res) {
    res.render('admin/formRecord');
});

router.post("/save", recordController.createRecord);

router.get("/", recordController.listRecord);

router.get("/:id", recordController.getRecord);


module.exports = router;
