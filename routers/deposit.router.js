const express = require('express');
var router = express.Router();
const paypal = require('paypal-rest-sdk');
var User = require("../models/user.model");

router.get('/', function (req, res) {
    res.render('client/deposit', {'user': req.user});
});

var total = 0;
router.post('/pay', function (req, res) {
    total = req.body.price;
    var item = [];
    item.push(req.body);
    console.log(item);
    const create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "https://dev20-doctor.herokuapp.com/deposit/success",
            "cancel_url": "https://dev20-doctor.herokuapp.com/deposit/cancel"
        },
        "transactions": [{
            "item_list": {
                "items": item
            },
            "amount": {
                "currency": "USD",
                "total": total
            },
            "description": "Hat for the best team ever"
        }]
    };
    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            console.log(error);
            res.render('client/cancle.ejs');
        } else {
            for (let i = 0; i < payment.links.length; i++) {
                if (payment.links[i].rel === 'approval_url') {
                    console.log(payment);
                    res.header("Access-Control-Allow-Origin", "*");
                    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                    res.send(payment.links[i].href);
                }
            }
        }
    });
});
router.get('/cancle', function (req, res) {
    res.render('client/cancle');
});
router.get('/success', (req, res) => {
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
    console.log(total)
    const execute_payment_json = {
        "payer_id": payerId,
        "transactions": [{
            "amount": {
                "currency": "USD",
                "total": total
            }
        }]
    };

    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            res.render('client/cancle');
        } else {
            console.log(JSON.stringify(payment));
            User.findById(req.user, function (err, list) {
                var newBalance = list.info.balance + parseFloat(total);
                User.findByIdAndUpdate(req.user, {"info.balance": newBalance}, {new: true}, function (err) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.redirect('/')
                    }
                });
            });
        }
    });
});

router.get('/cancel', (req, res) => res.send('Cancelled'));

module.exports = router;
