const express = require('express');
var doctorTimeController = require("../controllers/doctorTime.controller");
var router = express.Router();


router.get('/', function(req, res){
    res.render('client/formDoctorTime');
});
router.post("/save",doctorTimeController.createDoctorTime);

module.exports = router;
