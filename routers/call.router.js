const router = require('express').Router();
const User = require('../models/user.model');
var rateController = require("../controllers/rate.controller");
var callController = require("../controllers/call.controller");


router.get('/', (req, res) => {
    User.findById(req.query.friendId, function (err, friend) {
        res.render('client/call', {
            data: req.query,
            user: req.user,
            friend: {
                img: friend.info.img,
                name: friend.info.firstname + friend.info.lastname
            }
        });
    });


});
router.get('/rate', (req, res) => {
    res.render('client/rate', {user: req.user});
});
router.post('/rate/save', rateController.sendRate);


module.exports = router;
