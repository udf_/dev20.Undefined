const express = require('express');
var controller = require('../controllers/booking.controller');
var timeController = require('../controllers/time.controller');
var doctortimeController = require("../controllers/doctorTime.controller");
var router = express.Router();


router.post("/api/:id" , doctortimeController.doctorTimeBook);

router.get("/", timeController.listTimeBook);

router.post("/send", controller.sendBooking);

module.exports = router;
