const router = require('express').Router();
var Booking = require("../models/booking.model");
const Record = require('../models/record.model');
const moment = require('moment');
var mongoose = require('mongoose');
const User = require('../models/user.model');

var currenttime = moment().valueOf().toString();

router.get('/work_list', (req, res) => {
    Booking.find({time: {$gte: currenttime}}).sort({time: 1})
        .where('doctor').eq(new mongoose.mongo.ObjectId(req.user._id))
        .populate('user')
        .exec(function (err, list) {
            res.render('client/listWorkDoctor', {
                user: req.user,
                'listBooking': list
            });
        });
});

router.get('/form-record/:id', (req, res) => {
    Record.find({})
        .populate('user')
        .populate('doctor')
        .where('user').eq(new mongoose.mongo.ObjectId(req.params.id))
        .exec(function (err, list) {
            User.findById(req.params.id, function (err, doc1) {
                    // console.log(typeof doc1[0].info.dob);
                    res.render('client/doctor-form-record', {
                        user: req.user,
                        'listRecord': list,
                        victim: doc1,
                        dob: moment(doc1.info.dob).format('DD/MM/YYYY')
                    });
            });
        });
});

module.exports = router;
