const router = require('express').Router();
const diagnosisController = require('../controllers/diagnosis.controller');

router.route('/')
    .get(diagnosisController.generateSuggest);

router.route('/api/:locationId')
    .get(diagnosisController.processApiLocations);

router.route('/api/result')
    .post(diagnosisController.processApiResult);

router.route('/api/symptom/search')
    .get(diagnosisController.processApiSymptomSearch);

router.route('/api/disease/search')
    .get(diagnosisController.processApiDiseaseSearch);

router.route('/api/drug/search')
    .get(diagnosisController.processApiDrugSearch);

module.exports = router;
