const router = require('express').Router();
const accountController = require('../controllers/account.controller');

// account admin
router.route('/register/admin')
    .get(accountController.generateAccountAdminRegister)
    .post(accountController.checkInputAccountAdminRegister, accountController.processAccountCreate);

router.route('/login/admin')
    .get(accountController.generateAccountAdminLogin)
    .post(accountController.processAccountAdminLogin);

router.get('/logout/admin', function (req, res) {
    req.logout();
    res.redirect('/login/admin');
});

module.exports = router;
