const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const passport = require('passport');
const flash = require('connect-flash');
const validator = require('express-validator');
const cloudinary = require('cloudinary');
const fileUpload = require('express-fileupload');
const paypal = require('paypal-rest-sdk');
const exphdbs = require('express-handlebars');
const cors = require('cors');

mongoose.connect('mongodb://admin:admin123@ds157946.mlab.com:57946/dev20', {useNewUrlParser: true});
mongoose.connection.on('error', function (err) {
    console.log('Lỗi kết nối đến CSDL: ' + err);
});

//config cloudinary
cloudinary.config({
    cloud_name: 'dyi6c1dgi',
    api_key: '679713918597911',
    api_secret: 't3xiT15hfyCeBMMqZCM1YrVH3Hc'
});

require('./configs/passport');

const auth = require('./middleware/auth.middleware');

const adminRouter = require('./routers/admin.router');
const bookingRouter = require('./routers/booking.router');
const userRouter = require('./routers/user.router');
const otherRouter = require('./routers/other.router');
const recordRouter = require('./routers/record.router');
const doctorRouter = require('./routers/doctor.router');
const callRouter = require('./routers/call.router');
const suggestRouter = require('./routers/suggest.router');
const authenRouter = require('./routers/authentication.router');
const depositRouter = require('./routers/deposit.router');
const addScheduleRouter = require('./routers/addSchedule .router');

const app = express();

var post = process.env.PORT || 3002;

app.set('views', './views');
app.set('view engine', 'ejs');
/* Cấu hình passport */
app.use(session({
    secret: 'secured_key',
    resave: false,
    saveUninitialized: false
}));
paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'AauA-3dgIqS4M0J5nFHUJANPBLEStIKopUmoMB9x2ZHko9o11HeHLGLFP93ag-T3qdLJsC7iDntZAvXP',
    'client_secret': 'EJfeRJhW1OLvQvA1hL1V55y_U6qtJm7j_yrCZnYJCk1V5hIUaVDTk-7DMpJoxcda1eyrEITuZDJNma6T'
});

//some other code
app.use(validator());
app.use(flash());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('public'));
app.use(fileUpload());
app.use(cors());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    next();
});
app.use(authenRouter);
app.use('/admin/', auth.checkAdmin, adminRouter);
app.use('/doctor/', auth.checkDoctor, doctorRouter);
app.use('/user/', userRouter);
app.use('/record/', auth.checkAuthentication, recordRouter);
app.use('/addSchedule/', auth.checkDoctor, addScheduleRouter);
app.use('/suggest/', auth.checkAuthentication, auth.checkMoney ,suggestRouter);
app.use('/deposit/', auth.checkAuthentication, depositRouter);
app.use(otherRouter);
app.use('/call/', auth.checkAuthentication, callRouter);

app.use('/booking/', auth.checkAuthentication, bookingRouter);

var server = app.listen(post, () => console.log(`Chạy thành Công ở cổng ${post}`));
var io = require('socket.io').listen(server);
require('./controllers/call.controller')(io);
require('./controllers/sendMail.controller').cronSenMail();

// Trả lỗi 404 k tồn tại trang!!!!!
app.use(function (req, res, next) {
    var err = new Error('errHandle/404Err.ejs');
    err.status = 404;
    next(err);
});

// Trả lỗi 500
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render(err.message);
});





