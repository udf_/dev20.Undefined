let controller = require('../controllers/account.controller');
var User = require("../models/user.model");

module.exports = {
    checkAuthentication: function (req, res, next){
        if(req.isAuthenticated()){
            //req.isAuthenticated() will return true if user is logged in
            next();
        } else{
            res.redirect("/user/login");
        }
    },
    checkIsAdmin: function (req, res, next){
        if(req.isAuthenticated() && req.user.isAdmin){
            next();
        } else{
            res.render("errHandle/403Err.ejs", );
        }
    },
    checkAdmin: function (req, res, next){
        if(req.isAuthenticated() && req.user.isAdmin){
            req.accountName = req.user.info.firstname;
            req.img = req.user.info.img;
            next();
        } else{
            let messages = ['Please login to continue'];
            res.render('admin/login.ejs', {
                messages: messages,
                hasErrors: messages.length > 0,
                user: req.user
            });
        }
    },
    checkDoctor: function (req, res, next){
        if(req.isAuthenticated() && req.user.isDoctor){
            next();
        } else{
            res.redirect('/user/login');
        }
    } ,
    checkMoney: function (req, res, next){
        console.log(req.user.info.balance);
        if(req.user.info.balance >= 2){
            next();
        } else{
            res.redirect('/?alert=true');
        }
    }

};
